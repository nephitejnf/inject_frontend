import PySimpleGUI as sg

from inject_template import *
from inject_data import *
import os
from os import system as run
import shutil

class DangedApp:
    file_list = ["frontpic","sprite","backpic","cardpic","surfing","sword","fishing"]
    windblows = False
    rom_file = None
    cc_name = ""
    injected = False
    owsprite = ""
    surfer = ""
    fisher = ""
    sworder = ""
    frontpic = ""
    backpic = ""
    cardpic = ""
    pallet = ""
    command_final = ""

    def __init__(self):
        if os.name == 'nt': self.windblows = True


    #        link_video = Gtk.LinkButton("https://twitch.tv/nephitejnf", "TLDR; Video How do")
    #        tutorial.pack_start(link_video, True, True, 10)
    #        tutorial_label_player = Gtk.Label("Hopefully it is pretty simple to use and "
    #                "understand. The injector frontend just interfaces with the "
    #                "utility to make it easier on you. All you need to do is "
    #                "provide the rom and files to INJECT.\n\n"
    #                "First thing you need is the player sprites and pics, after "
    #                "that just click the appropriate buttons in the 'form'.\nOnce "
    #                "all of them are filled You can go ahead and fill in the rest "
    #                "of the info. Name and gender should be explanatory.\n"
    #                "Palette file is not required for a first-time injection.\n"
    #                "Picking a pack and color is not required.\n"
    #                "You may even use a full template to inject your player,\n"
    #                "all you need to do is point it to the template image."
    #                "Don't forget to fill in the rest of the boxes, though.")
    #        tutorial_label_player.set_line_wrap(True)
    #        tutorial.pack_start(tutorial_label_player, True, True, 0)
    #        tutorial_label_starter = Gtk.Label("Starter injection is the simplest "
    #                "Pick your species.\n"
    #                "After picking, you can decide to attach a held item of any "
    #                "kind as well.\n"
    #                "You can decide on DVs or leave them at all 0s so you can SR "
    #                "for shiny and torture your\n"
    #                "'NO_MOVE' is used to erase a move or leave a move slot "
    #                "empty, that way you can make a new starter if you had a "
    #                "previous one injected. "
    #                "Moveset is also able to be picked and you can have your "
    #                "starter as OP as you wish. The only thing not left to you "
    #                "disposal is level choice.")
    #        tutorial_label_starter.set_line_wrap(True)
    #        tutorial.pack_start(tutorial_label_starter, True, True, 0)
    #        tutorial_label_extra = Gtk.Label("Also, if you have an already injected rom "
    #                "you will be able to edit player info with it. The program "
    #                "will look for the flag when you inject for the first time.\n"
    #                "Injecting a starter may be a little more involved, "
    #                "in that you may need to redo some of you stuff to do make "
    #                "changes. The starter form is however simpler than the player "
    #                "form is.")
    #

    ###########
    #
    # UI Code? UIC
    #
    ###########
    starter_tab = [[sg.T('Species:'), sg.Drop(pokemon, key="pig_species")],
                   [sg.T('Held Item:'), sg.Drop(items, key='pig_item')],
                   [sg.T('DVs:'), sg.T("ATK"), sg.Spin([i for i in range(0, 16)], initial_value=0, key="dv_atk"), sg.T('DEF'), sg.Spin([i for i in range(0, 16)], initial_value=0, key="dv_def"), sg.T('SPE'), sg.Spin([i for i in range(0, 16)], initial_value=0, key="dv_spe"), sg.T("SPC"), sg.Spin([i for i in range(0, 16)], initial_value=0, key="dv_spc")],
                   [sg.T("Move 1:"), sg.Drop(moves, key="move1")],
                   [sg.T("Move 2:"), sg.Drop(moves, key="move2")],
                   [sg.T("Move 3:"), sg.Drop(moves, key="move3")],
                   [sg.T("Move 4:"), sg.Drop(moves, key="move4")],
                   [sg.RealtimeButton("INJECT", size=(60, 1), button_color=('white','#11CC22'), key="inject_start")]
                  ]

    player_tab = [[sg.T('Name:'), sg.In(size=(8,1), key=('pname'), change_submits=True, do_not_clear=True), sg.In(key='pal_file', do_not_clear=True), sg.FileBrowse(button_text='Palette File', target='pal_file')],
                    [sg.Radio('MALE', 'gender', key='MALE', default=True), sg.Radio('FEMALE', 'gender', key='FEMALE'), sg.Radio('NONE', 'gender', key='NONE')],
                    [sg.Image('', key='ow_sprite_img'), sg.Image('', key='front_pic_img')],
                    [sg.Button('Player Sprite', key='ow_sprite'), sg.Button('Front Pic', key='front_pic')],
                    [sg.Image('', key='surfer_img'), sg.Image('', key='back_pic_img')],
                    [sg.Button('Surf Sprite', key='surfer'), sg.Button('Back Pic', key='back_pic')],
                    [sg.Image('', key='fisher_img'), sg.Image('', key='card_pic_img')],
                    [sg.Button('Fishing Sprite', key='fisher'), sg.Button('Card Pic', key='card_pic')],
                    [sg.Image('', key='sworder_img')],
                    [sg.Button('Sword Sprite', key='sworder'), sg.Check('Enable Hairflip', key='hairflip')],
                    [sg.Button('Use Template File', key='template_inject')],
                    [sg.RealtimeButton('INJECT', size=(60, 1), button_color=('white','#11CC22'), key='inject_play')]]
    tabs_layout = [[sg.Tab('Starter INJECT', starter_tab), sg.Tab('Player INJECT', player_tab)]]

    pig_layout = [[sg.Button(button_text='Select CC Rom to INJECT', key='pig_select'), sg.In(key="pig_file")],
                [sg.TabGroup(tabs_layout)]
                ]
    window = sg.Window('INJECT pigs plz').Layout(pig_layout)

    ##############
    #
    # Dialog dumb stuff
    #
    ##############

    def change_butt_player(self):
        self.owsprite = sg.PopupGetFile('',file_types=(('PNG Image', "*.png"),("ALL FILES", "*.*"),), no_window=True)
        self.window.Element('ow_sprite_img').Update(self.owsprite)

    def change_surf_spr(self):
        self.surfer = sg.PopupGetFile('',file_types=(('PNG Image', "*.png"),("ALL FILES", "*.*"),), no_window=True)
        self.window.Element('surfer_img').Update(self.surfer)

    def change_fish_spr(self):
        self.fisher = sg.PopupGetFile('',file_types=(('PNG Image', "*.png"),("ALL FILES", "*.*"),), no_window=True)
        self.window.Element('fisher_img').Update(self.fisher)

    def change_sword_spr(self):
        self.sworder = sg.PopupGetFile('',file_types=(('PNG Image', "*.png"),("ALL FILES", "*.*"),), no_window=True)
        self.window.Element('sworder_img').Update(self.sworder)

    def change_front_pic(self):
        self.frontpic = sg.PopupGetFile('',file_types=(('PNG Image', "*.png"),("ALL FILES", "*.*"),), no_window=True)
        self.window.Element('front_pic_img').Update(self.frontpic)

    def change_back_pic(self):
        self.backpic = sg.PopupGetFile('',file_types=(('PNG Image', "*.png"),("ALL FILES", "*.*"),), no_window=True)
        self.window.Element('back_pic_img').Update(self.backpic)

    def change_card_pic(self):
        self.cardpic = sg.PopupGetFile('',file_types=(('PNG Image', "*.png"),("ALL FILES", "*.*"),), no_window=True)
        self.window.Element('card_pic_img').Update(self.cardpic)

    #########
    #
    # Some Sorta Functional Code
    #
    #########
    def INJECT(self):
        if not self.cc_name:
            sg.Popup("You haven't loaded a ceritified CC rom.", button_color="green")
        else:
            event, values = self.window.Read()
            command_final = "{}inject ".format("" if self.windblows else "./")
            if values['pname'] is not "":
                command_final += " -name {}".format(values['pname'])
            if values['MALE']:
                command_final += " -gender MALE"
            elif values['FEMALE']:
                command_final += " -gender FEMALE"
            elif values['NONE']:
                command_final += " -gender NONE"
            if values['hairflip']:
                command_final += " -flipenable"
            if frontpic is not "":
                command_final += " -frontpic "+ is_png(self.frontpic, "frontpic")
                if not pallet is "" and not self.injected:
                    command_final += " -palette output/palette.pal"
                print("front found")
            if self.pallet is not "":
                command_final += " -palette {}".format(self.pallet)
            if self.backpic is not "":
                command_final += " -backpic "+ is_png(self.backpic, "backpic")
                print("back found")
            if self.cardpic is not "":
                command_final += " -cardpic "+ is_png(self.cardpic, "cardpic")
                print("card found")
            if self.owsprite is not "":
                command_final += " -sprite "+ is_png(self.owsprite, "sprite")
                print("sprite found")
            if self.surfer is not "":
                command_final += " -surfing "+ is_png(self.surfer, "surfing")
                print("surf found")
            if self.fisher is not "":
                command_final += " -fishing "+ is_png(self.fisher, "fishing")
                print("fish found")
            if self.sworder is not "":
                command_final += " -sword "+ is_png(self.sworder, "sword")
                print("sword found")

        if self.prevent_inject():
            shutil.copyfile(self.cc_name, os.getcwd()+"/Crystal_Clear.gbc")
            run(command_final)
            self.fix_cc()
        else:
            print("Nothing to see")

    def prevent_inject(self):
        resp = None
        if not self.injected:
            resp = sg.PopupYesNo("You have a Clean CC rom.\nDid you make sure all the fields\nwere filled in?")
            if resp == 'Yes':
                resp = True
            elif resp == 'No':
                resp = False
        return(resp)

    def inject_tha_mon(self):
        args = []
        dumped, values = self.window.Read()
        if not cc_name:
            sg.Popup("You haven't loaded a certified pig CC rom!")
        else:
            if values['pig_species'] is not None:
                args.append("-species {}".format(values['pig_species']))
            if values['pig_item'] is not None:
                args.append("-item {}".format(values['pig_item']))
            for i in range(1, 5):
                if values["move{}".format(i)] is not None:
                    mover = values["move{}".format(i)]
                    args.append("-move{} {}".format(i, mover))
            args.append("-dvs {}{}{}{}".format(
                    self.create_hex(values['dv_atk']),
                    self.create_hex(values['dv_def']),
                    self.create_hex(values['dv_spe']),
                    self.create_hex(values['dv_spc'])))
            final = "{}inject ".format("" if windblows else "./")
            for thing in args:
                final += " " + thing
            run(final)
            self.fix_cc()

    def load_cc(self):
        self.rom_file
        self.rom_file = open(self.cc_name, "r+b")

        # post dialog processing
        self.injected = self.read_injected()
        if self.injected:
            shutil.copyfile(self.cc_name, os.getcwd()+"/Crystal_Clear.gbc")
        else:
            print('Not yet injected')
        self.rom_file.close()
        # visibility processing

    def read_injected(self):
        offset = int('0x1E8000', base=16)
        self.rom_file.seek(offset)
        temper = self.rom_file.read(1)
        temper = temper[-1]
        return int(temper)

    def create_hex(self, num):
        return hex(int(num)).split('x')[-1].upper()

    def from_template(self):
        templatte = sg.PopupGetFile('',file_types=(('PNG Image', "*.png"),("ALL FILES", "*.*"),), no_window=True)
        build_template(templatte)
        for piggo in file_list:
            is_png("input/{}.png".format(piggo), piggo)
        self.frontpic="input/frontpic.png"
        self.owsprite="input/sprite.png"
        self.backpic="input/backpic.png"
        self.cardpic="input/cardpic.png"
        self.surfer="input/surfing.png"
        self.sworder="input/sword.png"
        self.fisher="input/fishing.png"
        self.window.Element('ow_sprite_img').Update(self.owsprite)
        self.window.Element('surfer_img').Update(self.surfer)
        self.window.Element('fisher_img').Update(self.fisher)
        self.window.Element('sworder_img').Update(self.sworder)
        self.window.Element('front_pic_img').Update(self.frontpic)
        self.window.Element('back_pic_img').Update(self.backpic)
        self.window.Element('card_pic_img').Update(self.cardpic)

    def is_png(self, img, typer):
        self.is_pic = typer in ["cardpic", "backpic", "frontpic"]
        if img.endswith(".png"):
            if is_pic:
                if typer is "frontpic":
                    run("{}rgbgfx -p output/palette.pal {}".format(("" if self.windblows else "./"), img))
                run("{}rgbgfx -h -o output/{}.2bpp {}".format(("" if self.windblows else "./"), typer, img))
                run("{}lzcomp output/{}.2bpp output/{}2bpp.lz".format(("" if self.windblows else "./"), typer, typer))
                return("output/{}.2bpp.lz".format(typer))
            run("{}rgbgfx -o output/{}.2bpp {}".format(("" if self.windblows else "./"), typer, img))
            return("output/{}.2bpp".format(("" if self.windblows else "./"), typer))
        elif img.endswith(".2bpp"):
            if self.is_pic:
                run("{}lzcomp {} {}.2bpp.lz".format(("" if self.windblows else "./"), img, typer))
                return("output/{}.2bpp.lz".format(typer))
            return(img.format(typer))
        elif img.endswith(".2bpp.lz"):
            print(img)
            if not self.is_pic:
                print("Ummmmmm.... not a pic sprite")
                return None
            if self.is_pic: return(img.format(typer))

    def process_spr(self, spr, spr_type):
        run("{}rgbgfx -o output/{}.2bpp {}".format(("" if self.windblows else "./"), spr_type, spr))

    def compress_pic(self, pic, front):
        run("{}rgbgfx -h -o output/{}pic.2bpp {}".format(("" if self.windblows else "./"), front, pic))
        run("{}lzcomp -o output/{}pic.2bpp output/{}pic.2bpp.lz".format(("" if self.windblows else "./"), front, front))
        if front is "front":
            run("{}rgbgfx -p output/palette.pal {}".format(("" if self.windblows else "./"), pic))

    def fix_cc(self):
        run("{}rgbfix -f h ".format("" if self.windblows else "./") + "Crystal_Clear.gbc")
        run("{}rgbfix -f g ".format("" if self.windblows else "./") + "Crystal_Clear.gbc")

            # inject detect '0x1E8000'

    def main(self):
        while True:
            event, values = self.window.Read()
            if event == 'pig_select':
                self.cc_name = sg.PopupGetFile('Select dat pig', file_types=(("Crystal Clear ROM", "*.gbc"),("ALL Files", "*.*"),), no_window=True)
                self.window.Element('pig_file').Update(self.cc_name)
                self.load_cc()

            if len(values['pname']) > 7:
                self.window.Element('pname').Update(values['pname'][:-1])

            # That danged player inject sprites
            if event == 'ow_sprite':
                self.change_butt_player()
            if event == 'surfer':
                self.change_surf_spr()
            if event == 'sworder':
                self.change_sword_spr()
            if event == 'fisher':
                self.change_fish_spr()
            if event == 'front_pic':
                self.change_front_pic()
            if event == 'back_pic':
                self.change_back_pic()
            if event == 'card_pic':
                self.change_card_pic()
            if event == 'template_inject':
                self.from_template()

            # clean inject buttons
            if event == 'inject_start':
                self.inject_tha_mon()
            elif event == 'inject_play':
                self.INJECT()
            if values is None:
                break
            print(event, values)

        self.window.Close()

def main():
    app = DangedApp()
    app.main()

if __name__=='__main__':
    main()
