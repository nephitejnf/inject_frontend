import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

from inject_template import *
from inject_data import *
import os
from os import system as run
import shutil

class App(Gtk.Window):
    file_list = ["frontpic","sprite","backpic","cardpic","surfing","sword","fishing"]
    windblows = False
    rom_file = None
    cc_name = ""
    injected = False
    owsprite = ""
    surfer = ""
    fisher = ""
    sworder = ""
    frontpic = ""
    backpic = ""
    cardpic = ""
    pallet = ""
    #bloos = { 'front': False, 'back': False, 'card': False,
    #        'sprite': False, 'surf': False, 'fish': False, 'sword': False}

    def __init__(self):
        Gtk.Window.__init__(self, title="INJECT Pig")
        if os.name == 'nt': self.inblows = True
        header_pig = Gtk.HeaderBar()
        header_pig.set_show_close_button(True)
        header_pig.props.title = "INJECT Pig"
        self.set_titlebar(header_pig)
        self.rom_button = Gtk.Button("Select CC Rom to INJECT")
        self.rom_button.connect("clicked", self.load_cc)
        header_pig.pack_start(self.rom_button)

        # Book of books, or rather tab handling
        self.inject_book = Gtk.Notebook()
        self.add(self.inject_book)

        self.poke_box = Gtk.Box()
        self.poke_box.set_border_width(10)
        self.poke_inject_grid = Gtk.Grid()
        self.poke_box.add(self.poke_inject_grid)
        self.inject_book.append_page(self.poke_box,
                    Gtk.Label("Starter INJECT"))

        self.char_box = Gtk.Box()
        self.char_box.set_border_width(10)
        self.player_inject_grid = Gtk.Grid()
        self.player_inject_grid.set_column_homogeneous(True)
        self.char_box.add(self.player_inject_grid)
        self.inject_book.append_page(self.char_box, Gtk.Label("Player INJECT"))

        self.tutorial_scroll = Gtk.ScrolledWindow(None, None)
        self.tutorial = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=10)
        self.tutorial_scroll.add_with_viewport(self.tutorial)
        self.inject_book.append_page(self.tutorial_scroll, Gtk.Label("How Do? (Tutorials)"))

        link_video = Gtk.LinkButton("https://twitch.tv/nephitejnf", "TLDR; Video How do")
        self.tutorial.pack_start(link_video, True, True, 10)
        tutorial_label_player = Gtk.Label("Hopefully it is pretty simple to use and "
                "understand. The injector frontend just interfaces with the "
                "utility to make it easier on you. All you need to do is "
                "provide the rom and files to INJECT.\n\n"
                "First thing you need is the player sprites and pics, after "
                "that just click the appropriate buttons in the 'form'.\nOnce "
                "all of them are filled You can go ahead and fill in the rest "
                "of the info. Name and gender should be self-explanatory.\n"
                "Palette file is not required for a first-time injection.\n"
                "Picking a pack and color is not required.\n"
                "You may even use a full template to inject your player,\n"
                "all you need to do is point it to the template image."
                "Don't forget to fill in the rest of the boxes, though.")
        tutorial_label_player.set_line_wrap(True)
        self.tutorial.pack_start(tutorial_label_player, True, True, 0)
        tutorial_label_starter = Gtk.Label("Starter injection is the simplest "
                "Pick your species.\n"
                "After picking, you can decide to attach a held item of any "
                "kind as well.\n"
                "You can decide on DVs or leave them at all 0s so you can SR "
                "for shiny and torture yourself.\n"
                "'NO_MOVE' is used to erase a move or leave a move slot "
                "empty, that way you can make a new starter if you had a "
                "previous one injected. "
                "Moveset is also able to be picked and you can have your "
                "starter as OP as you wish. The only thing not left to you "
                "disposal is level choice.")
        tutorial_label_starter.set_line_wrap(True)
        self.tutorial.pack_start(tutorial_label_starter, True, True, 0)
        tutorial_label_extra = Gtk.Label("Also, if you have an already injected rom "
                "you will be able to edit player info with it. The program "
                "will look for the flag when you inject for the first time.\n"
                "Injecting a starter may be a little more involved, "
                "in that you may need to redo some of you stuff to do make "
                "changes. The starter form is however simpler than the player "
                "form is.")
        tutorial_label_extra.set_line_wrap(True)
        self.tutorial.pack_start(tutorial_label_extra, True, True, 0)



        ## Poke INJECT
        self.poke_species_label = Gtk.Label("Species:")
        self.poke_inject_grid.attach(self.poke_species_label, 0, 0, 1, 1)
        self.pokemon_species = Gtk.ComboBoxText()
        self.pokemon_species.set_entry_text_column(0)
        for poke_item in pokemon:
            self.pokemon_species.append_text(poke_item)
        self.poke_inject_grid.attach(self.pokemon_species, 1, 0, 3, 1)
        #self.pokemon_species.get_active_iter()
        self.item_label = Gtk.Label("Held item:")
        self.poke_inject_grid.attach(self.item_label, 0, 1, 1, 1)
        self.item_box = Gtk.ComboBoxText()
        self.item_box.set_entry_text_column(0)
        for item in items:
            self.item_box.append_text(item)
        self.poke_inject_grid.attach(self.item_box, 1, 1, 3, 1)
        #self.item_box.get_active_iter()
        self.atk_label = Gtk.Label("ATK")
        self.poke_inject_grid.attach(self.atk_label, 0, 2, 1, 1)
        self.def_label = Gtk.Label("DEF")
        self.poke_inject_grid.attach(self.def_label, 1, 2, 1, 1)
        self.spe_label = Gtk.Label("SPE")
        self.poke_inject_grid.attach(self.spe_label, 2, 2, 1, 1)
        self.spc_label = Gtk.Label("SPC")
        self.poke_inject_grid.attach(self.spc_label, 3, 2, 1, 1)
        dv_adjust1 = Gtk.Adjustment(0, 0, 15, 1, 1, 0)
        dv_adjust2 = Gtk.Adjustment(0, 0, 15, 1, 1, 0)
        dv_adjust3 = Gtk.Adjustment(0, 0, 15, 1, 1, 0)
        dv_adjust4 = Gtk.Adjustment(0, 0, 15, 1, 1, 0)
        self.dvs_atk = Gtk.SpinButton()
        self.dvs_atk.set_adjustment(dv_adjust1)
        self.poke_inject_grid.attach(self.dvs_atk, 0, 3, 1, 1)
        self.dvs_def = Gtk.SpinButton()
        self.dvs_def.set_adjustment(dv_adjust2)
        self.poke_inject_grid.attach(self.dvs_def, 1, 3, 1, 1)
        self.dvs_spe = Gtk.SpinButton()
        self.dvs_spe.set_adjustment(dv_adjust3)
        self.poke_inject_grid.attach(self.dvs_spe, 2, 3, 1, 1)
        self.dvs_spc = Gtk.SpinButton()
        self.dvs_spc.set_adjustment(dv_adjust4)
        self.poke_inject_grid.attach(self.dvs_spc, 3, 3, 1, 1)
        #self.dvs_.get_value_as_int()
        self.move_box = {}
        self.move_label = {}
        for i in range(1, 5):
            self.move_label["{}".format(i)] = Gtk.Label("Move {}:".format(i))
            self.move_box["{}".format(i)] = Gtk.ComboBoxText()
            self.move_box["{}".format(i)].set_entry_text_column(0)
            for move in moves:
                self.move_box["{}".format(i)].append_text(move)
            self.poke_inject_grid.attach(self.move_label["{}".format(i)],
                            0, 3 + i, 1, 1)
            self.poke_inject_grid.attach(self.move_box["{}".format(i)],
                            1, 3 + i, 3, 1)



        ## Player INJECT
        # inject detect '0x1E8000'
        self.name_butt = Gtk.Entry()
        self.name_butt.set_max_length(7)
        self.name_butt.set_placeholder_text("Name")
        self.player_inject_grid.attach(self.name_butt, 0, 0, 1, 1)
        # palette
        self.pal_box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        self.pal_butt = Gtk.Button("Palette File")
        self.pal_butt.connect("clicked", self.pallet_get)
        self.pal_box.add(self.pal_butt)
        self.pal_label = Gtk.Label("")
        self.pal_box.add(self.pal_label)
        self.player_inject_grid.attach(self.pal_box, 1, 0, 1, 1)
        # gendering
        self.gender_box = Gtk.Box(spacing=6)
        self.gender_male = Gtk.RadioButton.new_with_label_from_widget(None, "MALE")
        self.gender_female = Gtk.RadioButton.new_with_label_from_widget(self.gender_male, "FEMALE")
        self.gender_none = Gtk.RadioButton.new_with_label_from_widget(self.gender_male, "NONE")
        self.gender_box.add(self.gender_male)
        self.gender_box.add(self.gender_female)
        self.gender_box.add(self.gender_none)
        self.player_inject_grid.attach(self.gender_box, 0, 1, 2, 1)

        # player ow
        self.player_butt = Gtk.Button("Player Sprite")
        self.player_butt.connect("clicked", self.change_butt_player)
        self.player_inject_grid.attach(self.player_butt, 0, 2, 1, 1)
        self.player_spr  = Gtk.Image()
        self.player_inject_grid.attach(self.player_spr, 0, 3, 1, 1)
        # surfer
        self.player_surf = Gtk.Button("Surf Sprite")
        self.player_surf.connect("clicked", self.change_surf_spr)
        self.player_inject_grid.attach(self.player_surf, 0, 4, 1, 1)
        self.surf_spr = Gtk.Image()
        self.player_inject_grid.attach(self.surf_spr, 0, 5, 1, 1)
        # fish
        self.player_fish = Gtk.Button("Fishing Sprite")
        self.player_fish.connect("clicked", self.change_fish_spr)
        self.player_inject_grid.attach(self.player_fish, 0, 6, 1, 1)
        self.fish_spr = Gtk.Image()
        self.player_inject_grid.attach(self.fish_spr, 0, 7, 1, 1)
        # sword
        self.player_sword = Gtk.Button("Sword Sprite")
        self.player_sword.connect("clicked", self.change_sword_spr)
        self.player_inject_grid.attach(self.player_sword, 0, 8, 1, 1)
        self.sword_spr = Gtk.Image()
        self.player_inject_grid.attach(self.sword_spr, 0, 9, 1, 1)
        # front
        self.front_pic_butt = Gtk.Button("Front Pic")
        self.front_pic_butt.connect("clicked", self.change_front_pic)
        self.player_inject_grid.attach(self.front_pic_butt, 1, 2, 1, 1)
        self.front_pic  = Gtk.Image()
        self.player_inject_grid.attach(self.front_pic, 1, 3, 1, 1)
        # back
        self.back_pic_butt = Gtk.Button("Back Pic")
        self.back_pic_butt.connect("clicked", self.change_back_pic)
        self.player_inject_grid.attach(self.back_pic_butt, 1, 4, 1, 1)
        self.back_pic  = Gtk.Image()
        self.player_inject_grid.attach(self.back_pic, 1, 5, 1, 1)
        # card
        self.card_pic_butt = Gtk.Button("Card Pic")
        self.card_pic_butt.connect("clicked", self.change_card_pic)
        self.player_inject_grid.attach(self.card_pic_butt, 1, 6, 1, 1)
        self.card_pic  = Gtk.Image()
        self.player_inject_grid.attach(self.card_pic, 1, 7, 1, 1)
        self.hairflip = Gtk.CheckButton.new_with_label("Enable Hairflip")
        self.player_inject_grid.attach(self.hairflip, 0, 10, 2, 1)
        self.template_file = Gtk.Button("Use Template File")
        self.template_file.connect("clicked", self.from_template)
        self.player_inject_grid.attach(self.template_file, 0, 11, 2, 1)
        # button player inject
        self.inject_player_butt = Gtk.Button("INJECT")
        self.inject_player_butt.connect("clicked", self.INJECT)
        self.player_inject_grid.attach(self.inject_player_butt, 0, 13, 2, 1)


        ## INJECT Buttons
        self.button = Gtk.Button(label="INJECT")
        self.button.connect("clicked", self.on_button_clicked)
        self.poke_inject_grid.attach(self.button, 1, 10, 2, 1)

    # copypasta this for other sprites
    def pallet_get(self, butt):
        dialog = Gtk.FileChooserDialog("Select you pallet", self,
                Gtk.FileChooserAction.OPEN,
                (Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
                    Gtk.STOCK_OPEN, Gtk.ResponseType.OK))
        response = dialog.run()
        if response == Gtk.ResponseType.OK:
            self.pallet = dialog.get_filename()
            self.pal_label.set_text(dialog.get_filename().split('/')[-1])
        elif response == Gtk.ResponseType.CANCEL:
            print("nada")
        dialog.destroy()

    def change_butt_player(self, butt):
        dialog = Gtk.FileChooserDialog("Select you Player Sprite", self,
                Gtk.FileChooserAction.OPEN,
                (Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
                    Gtk.STOCK_OPEN, Gtk.ResponseType.OK))
        self.add_png_filters(dialog)
        response = dialog.run()
        if response == Gtk.ResponseType.OK:
            self.player_spr.set_from_file(dialog.get_filename())
            self.owsprite = dialog.get_filename()
        elif response == Gtk.ResponseType.CANCEL:
            print("nada")
        dialog.destroy()

    def change_surf_spr(self, butt):
        dialog = Gtk.FileChooserDialog("Select you surf Sprite", self,
                Gtk.FileChooserAction.OPEN,
                (Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
                    Gtk.STOCK_OPEN, Gtk.ResponseType.OK))
        self.add_png_filters(dialog)
        response = dialog.run()
        if response == Gtk.ResponseType.OK:
            self.surf_spr.set_from_file(dialog.get_filename())
            self.surfer = dialog.get_filename()
        elif response == Gtk.ResponseType.CANCEL:
            print("nada")
        dialog.destroy()

    def change_fish_spr(self, butt):
        dialog = Gtk.FileChooserDialog("Select you fishing Sprite", self,
                Gtk.FileChooserAction.OPEN,
                (Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
                    Gtk.STOCK_OPEN, Gtk.ResponseType.OK))
        self.add_png_filters(dialog)
        response = dialog.run()
        if response == Gtk.ResponseType.OK:
            self.fish_spr.set_from_file(dialog.get_filename())
            self.fisher = dialog.get_filename()
        elif response == Gtk.ResponseType.CANCEL:
            print("nada")
        dialog.destroy()

    def change_sword_spr(self, butt):
        dialog = Gtk.FileChooserDialog("Select you sword Sprite", self,
                Gtk.FileChooserAction.OPEN,
                (Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
                    Gtk.STOCK_OPEN, Gtk.ResponseType.OK))
        self.add_png_filters(dialog)
        response = dialog.run()
        if response == Gtk.ResponseType.OK:
            self.sword_spr.set_from_file(dialog.get_filename())
            self.sworder = dialog.get_filename()
        elif response == Gtk.ResponseType.CANCEL:
            print("nada")
        dialog.destroy()

    def change_front_pic(self, butt):
        dialog = Gtk.FileChooserDialog("Select you front pic", self,
                Gtk.FileChooserAction.OPEN,
                (Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
                    Gtk.STOCK_OPEN, Gtk.ResponseType.OK))
        self.add_png_filters(dialog)
        response = dialog.run()
        if response == Gtk.ResponseType.OK:
            self.front_pic.set_from_file(dialog.get_filename())
            self.frontpic = dialog.get_filename()
        elif response == Gtk.ResponseType.CANCEL:
            print("nada")
        dialog.destroy()

    def change_back_pic(self, butt):
        dialog = Gtk.FileChooserDialog("Select you back pic", self,
                Gtk.FileChooserAction.OPEN,
                (Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
                    Gtk.STOCK_OPEN, Gtk.ResponseType.OK))
        self.add_png_filters(dialog)
        response = dialog.run()
        if response == Gtk.ResponseType.OK:
            self.back_pic.set_from_file(dialog.get_filename())
            self.backpic = dialog.get_filename()
        elif response == Gtk.ResponseType.CANCEL:
            print("nada")
        dialog.destroy()

    def change_card_pic(self, butt):
        dialog = Gtk.FileChooserDialog("Select you card pic", self,
                Gtk.FileChooserAction.OPEN,
                (Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
                    Gtk.STOCK_OPEN, Gtk.ResponseType.OK))
        self.add_png_filters(dialog)
        response = dialog.run()
        if response == Gtk.ResponseType.OK:
            self.card_pic.set_from_file(dialog.get_filename())
            self.cardpic = dialog.get_filename()
        elif response == Gtk.ResponseType.CANCEL:
            print("nada")
        dialog.destroy()


    def add_png_filters(self, dialog):
        filter_png = Gtk.FileFilter()
        filter_png.set_name("PNG Images")
        filter_png.add_mime_type("image/png")
        dialog.add_filter(filter_png)

    def add_txt_filter(self, dialog):
        filter_txt = Gtk.FileFilter()
        filter_txt.set_name("Plain Text")
        filter_txt.add_mime_type("text/plain")
        dialog.add_filter(filter_png)

    def process_spr(self, spr, spr_type):
        run("{}rgbgfx -o output/{}.2bpp {}".format(("" if self.windblows else "./"), spr_type, spr))

    def compress_pic(self, pic, front):
        run("{}rgbgfx -h -o output/{}pic.2bpp {}".format(("" if self.windblows else "./"), front, pic))
        run("{}lzcomp -o output/{}pic.2bpp output/{}pic.2bpp.lz".format(("" if self.windblows else "./"), front, front))
        if front is "front":
            run("{}rgbgfx -p output/palette.pal {}".format(("" if self.windblows else "./"), pic))
        #run()

    def INJECT(self, butt):
        if not self.cc_name:
            dialog = Gtk.MessageDialog(self, 0, Gtk.MessageType.WARNING,
                    Gtk.ButtonsType.OK, "NO PIGS LOADED")
            dialog.format_secondary_text(
                    "You haven't loaded a certified by pig CC Rom!")
            resp = dialog.run()
            dialog.destroy()
            return(resp)
        command_final = "{}inject ".format("" if self.windblows else "./")
        if self.name_butt.get_text() is not "":
            command_final += " -name {}".format(self.name_butt.get_text())
        if self.gender_male.get_active():
            command_final += " -gender MALE"
        elif self.gender_female.get_active():
            command_final += " -gender FEMALE"
        else:
            command_final += " -gender NONE"
        if self.hairflip.get_active():
            command_final += " -flipenable"
        if self.frontpic is not "":
            command_final += " -frontpic "+ self.is_png(self.frontpic, "frontpic")
            if not self.pallet is "" and not injected:
                command_final += " -palette output/palette.pal"
            print("front found")
        if self.pallet is not "":
            command_final += " -palette {}".format(self.pallet)
        if self.backpic is not "":
            command_final += " -backpic "+ self.is_png(self.backpic, "backpic")
            print("back found")
        if self.cardpic is not "":
            command_final += " -cardpic "+ self.is_png(self.cardpic, "cardpic")
            print("card found")
        if self.owsprite is not "":
            command_final += " -sprite "+ self.is_png(self.owsprite, "sprite")
            print("sprite found")
        if self.surfer is not "":
            command_final += " -surfing "+ self.is_png(self.surfer, "surfing")
            print("surf found")
        if self.fisher is not "":
            command_final += " -fishing "+ self.is_png(self.fisher, "fishing")
            print("fish found")
        if self.sworder is not "":
            command_final += " -sword "+ self.is_png(self.sworder, "sword")
            print("sword found")
        #if self.pack_spin.get_value_as_int() is not 0:
        #    command_final += " -pack {}".format(self.pack_spin.get_value_as_int())
        #    print("found pack")
        #if self.pack_com.get_active_text() is not None:
        #    command_final += " -color {}".format(self.pack_com.get_active_text())
        #    print("found color")


        if self.prevent_inject():
            shutil.copyfile(self.cc_name, os.getcwd()+"/Crystal_Clear.gbc")
            run(command_final)
            self.fix_cc()
        else:
            print("Nothing to see")

    def prevent_inject(self):
        resp = None
        if not self.injected:
            dialog = Gtk.MessageDialog(self, 0, Gtk.MessageType.WARNING,
                    Gtk.ButtonsType.YES_NO, "Clean ROM No INJECTION")
            dialog.format_secondary_text(
                    "It looks like this rom is clean.\nDid you make sure all the fields on the\nplayer inject were filled in?")
            resp = dialog.run()
            if resp == Gtk.ResponseType.YES:
                resp = True
            elif resp == Gtk.ResponseType.NO:
                resp = False
            dialog.destroy()
        return(resp)

    def is_png(self, img, typer):
        is_pic = typer in ["cardpic", "backpic", "frontpic"]
        if img.endswith(".png"):
            if is_pic:
                if typer is "frontpic":
                    run("{}rgbgfx -p output/palette.pal {}".format(("" if self.windblows else "./"), img))
                run("{}rgbgfx -h -o output/{}.2bpp {}".format(("" if self.windblows else "./"), typer, img))
                run("{}lzcomp output/{}.2bpp output/{}2bpp.lz".format(("" if self.windblows else "./"), typer, typer))
                return("output/{}.2bpp.lz".format(typer))
            run("{}rgbgfx -o output/{}.2bpp {}".format(("" if self.windblows else "./"), typer, img))
            return("output/{}.2bpp".format(typer))
        elif img.endswith(".2bpp"):
            if is_pic:
                run("{}lzcomp {} {}.2bpp.lz".format(("" if self.windblows else "./"), img, typer))
                return("output/{}.2bpp.lz".format(typer))
            return(img.format(typer))
        elif img.endswith(".2bpp.lz"):
            print(img)
            if not is_pic:
                print("Ummmmmm.... not a pic sprite")
                return None
            if is_pic: return(img.format(typer))

    def from_template(self, butt):
        dialog = Gtk.FileChooserDialog("Select you template", self,
                Gtk.FileChooserAction.OPEN,
                (Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
                    Gtk.STOCK_OPEN, Gtk.ResponseType.OK))
        response = dialog.run()
        if response == Gtk.ResponseType.OK:
            build_template(dialog.get_filename())
        elif response == Gtk.ResponseType.CANCEL:
            print("nada")
        dialog.destroy()
        for piggo in self.file_list:
            self.is_png("input/{}.png".format(piggo), piggo)
        self.frontpic="input/frontpic.png"
        self.owsprite="input/sprite.png"
        self.backpic="input/backpic.png"
        self.cardpic="input/cardpic.png"
        self.surfer="input/surfing.png"
        self.sworder="input/sword.png"
        self.fisher="input/fishing.png"
        self.player_spr.set_from_file(self.owsprite)
        self.surf_spr.set_from_file(self.surfer)
        self.fish_spr.set_from_file(self.fisher)
        self.sword_spr.set_from_file(self.sworder)
        self.front_pic.set_from_file(self.frontpic)
        self.back_pic.set_from_file(self.backpic)
        self.card_pic.set_from_file(self.cardpic)

    def on_button_clicked(self, widget):
        args = []
        if not self.cc_name:
            dialog = Gtk.MessageDialog(self, 0, Gtk.MessageType.WARNING,
                    Gtk.ButtonsType.OK, "NO PIGS LOADED")
            dialog.format_secondary_text(
                    "You haven't loaded a certified by pig CC Rom!")
            resp = dialog.run()
            dialog.destroy()
            return(resp)
        if self.pokemon_species.get_active_text() is not None:
            args.append("-species {}".format(self.pokemon_species.get_active_text()))
        if self.item_box.get_active_text() is not None:
            args.append("-item {}".format(self.item_box.get_active_text()))
        for i in range(1, 5):
            if self.move_box["{}".format(i)].get_active_text() is not None:
                mover = self.move_box["{}".format(i)].get_active_text()
                args.append("-move{} {}".format(i, mover))
        args.append("-dvs {}{}{}{}".format(
                self.create_hex(self.dvs_atk),
                self.create_hex(self.dvs_def),
                self.create_hex(self.dvs_spe),
                self.create_hex(self.dvs_spc)))
        final = "{}inject ".format("" if self.windblows else "./")
        shutil.copyfile(self.cc_name, os.getcwd()+"/Crystal_Clear.gbc")
        for thing in args:
            final += " " + thing
        run(final)
        self.fix_cc()
        #run(final)

    def fix_cc(self):
        run("{}rgbfix -f h ".format("" if self.windblows else "./") + "Crystal_Clear.gbc")
        run("{}rgbfix -f g ".format("" if self.windblows else "./") + "Crystal_Clear.gbc")

    def load_cc(self, butt):
        dialog = Gtk.FileChooserDialog("Select you CC Rom", self,
                Gtk.FileChooserAction.OPEN,
                (Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
                    Gtk.STOCK_OPEN, Gtk.ResponseType.OK))
        response = dialog.run()
        if response == Gtk.ResponseType.OK:
            self.cc_name = dialog.get_filename()
            self.rom_file = open(self.cc_name, "r+b")
        elif response == Gtk.ResponseType.CANCEL:
            print("nada")
        dialog.destroy()

        # post dialog processing
        self.injected = self.read_injected()
        self.rom_file.close()
        # visibility processing

    def read_injected(self):
        offset = int('0x1E8000', base=16)
        self.rom_file.seek(offset)
        temper = self.rom_file.read(1)
        temper = temper[-1]
        return int(temper)

    def create_hex(self, num):
        return hex(num.get_value_as_int()).split('x')[-1].upper()

win = App()
win.connect("destroy", Gtk.main_quit)
win.show_all()
Gtk.main()
