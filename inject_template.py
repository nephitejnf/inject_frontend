from PIL import Image

BLUE  = 2
GREEN = 3

# SPRITE DOTS
# KEY   |  BLUE     | GREEN
dic_pix = {
"front": [(152,92) ,(209,149)],
"back" : [(227,100),(276,149)],
"card" : [(295,92) ,(336,149)],
"sprite":[(374,52) ,(391,149)],
"surf"  :[(437,52) ,(454,149)],
"sword" : [(569,140),(642,149)],
"fishing":[(480,140),(553,149)]
}

sprite_dic = {
        "frontpic":[(153, 93, 209, 149),None],
        "sprite":[(375, 53, 391, 149),None],
        "backpic":[(228, 101, 276, 149),None],
        "cardpic":[(296, 93, 336, 149),None],
        "surfing":[(438, 53, 454, 149),None],
        "sword":[(570, 141, 642, 149),None],
        "fishing":[(481, 141, 553, 149),None]
}

# create the template image
def build_template(pigname):
    full_template = Image.open(pigname)
    #if verify_template(full_template):
    for key in sprite_dic:
        print("processing {}".format(key))
        process_file(key, full_template)

def verify_template(templ):
    for key in dic_pix:
        top = BLUE is templ.getpixel(dic_pix[key][0])
        bottom = GREEN is templ.getpixel(dic_pix[key][1])
        if top and bottom:
            print("{} VERIFIED!".format(key.upper()))
        else:
            return False

    print("TEMPLATE IS GOOD TEMPLATE")
    return True

def process_file(diclist, full_template):
    spr_temp = full_template.crop(sprite_dic[diclist][0])
    sprite_dic[diclist][1] = spr_temp.convert("RGBA", colors=4)
    sprite_dic[diclist][1].save("input/{}.png".format(diclist))
